const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get("/posts", (req, res) => {});

app.post("/posts", (req, res) => {});

app.listen(4002, () => console.log("listening on 4002"));
